<?php

use App\Livewire\Pages\HomePage;
use App\Livewire\Pages\UserPage;
use App\Livewire\SPANavigate;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
 */

Route::get('/', function () {
    return view('welcome');
});

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified',
])->group(function () {
    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');
});
Route::get('test', function () {
    return view('test');
})->name('test');
Route::get('task', function () {
    return view('task');
})->name('task')->middleware('auth');
Route::get('users', function () {
    return view('users.index');
})->name('users')->middleware('auth');
Route::get('user-page/{user}', UserPage::class)->name('user.page')->middleware('auth');
Route::get('home-page', HomePage::class)->name('home.page')->middleware('auth');

Route::get('modales', function () {
    return view('modales.index');
})->name('modales')->middleware('auth');
Route::get('tablas', function () {
    return view('tablas.index');
})->name('tablas')->middleware('auth');
Route::get('landing', SPANavigate::class)->name('landing')->middleware('auth');
