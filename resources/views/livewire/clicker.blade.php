<div class="m-4">
    @if (session('success'))
        <span class="block px-3 py-3 bg-green-600 text-white rounded">{{ session('success') }}</span>
    @endif
    <h1>Total de {{ $title }}: {{ $users->count() }}</h1>
    <form wire:submit.prevent="createNewUser">
        <label for="name"></label>
        Nombre
        <input type="text" id="name" class="block rounded border-gray-100 px-3 py-1 mb-1"
            wire:model.live="name"></input>
        @error('name')
            <small class="text-red-500 block">{{ $message }}</small>
        @enderror
        <label for="email"></label>
        Correo
        <input type="email" id="email" class="block rounded border-gray-100 px-3 py-1 mb-1"
            wire:model.live="email"></input>

        @error('email')
            <small class="text-red-500 block">{{ $message }}</small>
        @enderror
        <label for="password"></label>
        contraseña
        <input type="password" id="password" class="block rounded border-gray-100 px-3 py-1 mb-2"
            wire:model.live="password"></input>

        @error('password')
            <small class="text-red-500 block">{{ $message }}</small>
        @enderror

        <label class="mt-3 block text-sm font-medium leading-6 text-gray-900">Foto de perfil</label>
        <input wire:model="profile_photo_path" accept="image/png, image/jpeg" type="file" id="image"
            class="ring-1 ring-inset ring-gray-300 bg-gray-100 text-gray-900 text-sm rounded block w-full mb-3 ">
        @error('profile_photo_path')
            <span class="text-red-500 text-xs">{{ $message }} </span>
        @enderror
        <div wire:loading wire:target="profile_photo_path">
            <span class="text-green-500">Cargando foto ...</span>
        </div>
        <div wire:loading.delay.longest>
            <span class="text-green-500">Guardando!</span>
        </div>

        @if ($profile_photo_path)
            <img class="rounded w-20 h-20 my-5 block" src="{{ $profile_photo_path->temporaryUrl() }}" alt="">
        @endif

        <button wire:loading.class="bg-gray-200" wire:loading.attr="disabled" type="submit"
            class="block rounded px-3 py-1 bg-gray-600 text-white">REGISTRAR</button>
    </form>

    <hr class="my-4">
    @foreach ($users as $user)
        <ul>
            <li>{{ $user->name }}</li>
        </ul>
    @endforeach
    <div>
        {{ $users->links() }}
    </div>
</div>
