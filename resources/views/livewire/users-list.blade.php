<div class="relative overflow-x-auto shadow-md sm:rounded-lg">
    {{-- <div class="relative overflow-x-auto shadow-md sm:rounded-lg" wire:poll.keep-alive.2s> para actualizacion automático impide que funcione correctamente el blur o live --}}
    {{-- ESTADOS SIN CONEXIÓN --}}
    <div class="bg-white" wire:offline>
        <div class="mx-auto max-w-7xl py-12 px-2 sm:px-6 lg:px-8">
            <div class="mx-auto max-w-4xl">
                <div class="rounded-md bg-red-50 p-4">
                    <div class="flex">
                        <div class="flex-shrink-0">
                            <svg class="h-5 w-5 text-red-400" x-description="Heroicon name: mini/check-circle" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor"
                                aria-hidden="true">
                                <path fill-rule="evenodd"
                                    d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.857-9.809a.75.75 0 00-1.214-.882l-3.483 4.79-1.88-1.88a.75.75 0 10-1.06 1.061l2.5 2.5a.75.75 0 001.137-.089l4-5.5z"
                                    clip-rule="evenodd"></path>
                            </svg>
                        </div>
                        <div class="ml-3">
                            <p class="text-sm font-medium text-red-800">UPS! no tiene conexión a internet pague su conexión oe</p>
                        </div>
                        <div class="ml-auto pl-3">
                            <div class="-mx-1.5 -my-1.5">
                                <button type="button"
                                    class="inline-flex rounded-md bg-red-50 p-1.5 text-red-500 hover:bg-red-100 focus:outline-none focus:ring-2 focus:ring-red-600 focus:ring-offset-2 focus:ring-offset-red-50">
                                    <span class="sr-only">Cerrar</span>
                                    <svg class="h-5 w-5" x-description="Heroicon name: mini/x-mark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                        <path
                                            d="M6.28 5.22a.75.75 0 00-1.06 1.06L8.94 10l-3.72 3.72a.75.75 0 101.06 1.06L10 11.06l3.72 3.72a.75.75 0 101.06-1.06L11.06 10l3.72-3.72a.75.75 0 00-1.06-1.06L10 8.94 6.28 5.22z">
                                        </path>
                                    </svg>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="pb-4 bg-white dark:bg-gray-900 m-2">
        <label for="table-search" class="sr-only">Buscar</label>
        <div class="relative mt-1">
            <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                <svg class="w-4 h-4 text-gray-500 dark:text-gray-400" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 20 20">
                    <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="m19 19-4-4m0-7A7 7 0 1 1 1 8a7 7 0 0 1 14 0Z" />
                </svg>
            </div>
            <input wire:model.live.debounce.600ms="search" type="search" id="table-search"
                class="block p-2 pl-10 text-sm text-gray-900 border border-gray-300 rounded-lg w-80 bg-gray-50 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                placeholder="Buscar usuario" wire:offline.attr="disabled">
        </div>
    </div>
    <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
        <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
            <tr>
                <th scope="col" class="px-6 py-3">
                    ID
                </th>
                <th scope="col" class="px-6 py-3">
                    Foto
                </th>
                <th scope="col" class="px-6 py-3">
                    Nombre
                </th>
                <th scope="col" class="px-6 py-3">
                    Registrado
                </th>
                <th scope="col" class="px-6 py-3" wire:offline.remove>
                    Acciones
                </th>
            </tr>
        </thead>
        <tbody>
            @forelse ($this->users as $user)
            <tr wire:key={{ $user->id }} class="bg-white border-b dark:bg-gray-900 dark:border-gray-700">
                <th scope="row" class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                    {{ $user->id }}
                </th>
                <td class="px-6 py-4">
                    <img class="w-10 h-10 rounded-full" src="{{ isset($user->profile_photo_path) ? Storage::disk('public')->url($user->profile_photo_path) : asset('assets/img/not-photo.png') }}"
                        alt="{{ $user->name }}">
                </td>

                {{-- <th scope="row"
                        class="flex items-center px-6 py-4 text-gray-900 whitespace-nowrap dark:text-white">
                        <img class="w-10 h-10 rounded-full" src="/docs/images/people/profile-picture-1.jpg"
                            alt="Jese image">
                        <div class="pl-3">
                            <div class="text-base font-semibold">Neil Sims</div>
                            <div class="font-normal text-gray-500">neil.sims@flowbite.com</div>
                        </div>
                    </th> --}}
                <td class="px-6 py-4">
                    {{ $user->name }}
                </td>
                <td class="px-6 py-4">
                    {{ $user->created_at->format('d/m/Y') }}
                </td>
                <td class="px-6 py-4" wire:offline.remove>
                    <button wire:click="viewUser({{ $user }})" type="button" class="font-medium text-blue-600 dark:text-blue-500 hover:underline">Ver</button>
                </td>
            </tr>

            @empty
            <tr>
                <td colspan="5" class="text-center">No existe registros</td>
            </tr>
            @endforelse

        </tbody>
    </table>
    <nav wire:offline.remove class="flex items-center justify-between p-4" aria-label="Table navigation">
        {{ $this->users->links() }}
    </nav>
    @if(isset($selectedUser))
    <x-modal-custom name="user-details" title="Ver Usuario">
        <x-slot:content>
            Usuario: {{ $user->name }}<br>
            Correo: {{ $user->email }}<br>
            Registrado: {{ $user->created_at->format('d-m-Y') }}<br>
        </x-slot:content>
    </x-modal-custom>
    @endif
</div>
