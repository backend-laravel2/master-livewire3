<div id="content" class="mx-auto" style="max-width:500px;">
    {{-- Alert --}}
    @if (session()->has('alert'))
        <div class="bg-{{ session('alert')['color'] }}-100 border-t-4 border-{{ session('alert')['color'] }}-500 rounded-b text-{{ session('alert')['color'] }}-900 px-4 py-3 shadow-md"
            role="alert">
            <div class="flex">
                <div class="py-1"><svg class="fill-current h-6 w-6 text-{{ session('alert')['color'] }}-500 mr-4"
                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                        <path
                            d="M2.93 17.07A10 10 0 1 1 17.07 2.93 10 10 0 0 1 2.93 17.07zm12.73-1.41A8 8 0 1 0 4.34 4.34a8 8 0 0 0 11.32 11.32zM9 11V9h2v6H9v-4zm0-6h2v2H9V5z" />
                    </svg></div>
                <div>
                    <p class="font-bold">{{ session('alert')['title'] }}</p>
                    <p class="text-sm">{{ session('alert')['message'] }}</p>
                </div>
            </div>
        </div>
    @endif
    @include('livewire.includes.create-todo-box')
    @include('livewire.includes.search-box')
    <div id="todos-list">
        @forelse ($todos as $todo)
            @include('livewire.includes.todo-card')
        @empty
            Sin tareas
        @endforelse

        <div class="my-2">
            <!-- Pagination goes here -->
            {{ $todos->links() }}
        </div>
    </div>
</div>
