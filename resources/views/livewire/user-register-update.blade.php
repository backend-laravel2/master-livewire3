<div class="m-4" wire:keydown.shift.space.window="create">
    {{-- <div class="m-4" wire:keydown.space.window="create"> sin combinar teclas --}}
    <!-- Code block starts -->
    @if (session()->has('alert'))
    <div class="flex items-center justify-center px-4 w-full" wire:ignore>
        <div role="alert" id="alert"
            class="transition duration-150 ease-in-out w-full lg:w-full mx-auto bg-white dark:bg-gray-800 shadow rounded flex flex-col py-4 md:py-0 items-center md:flex-row justify-between">
            <div class="flex flex-col items-center md:flex-row">
                <div class="mr-3 p-4 bg-{{ session('alert')['color'] }}-400 rounded md:rounded-tr-none md:rounded-br-none text-white">
                    <img class="focus:outline-none" src="https://tuk-cdn.s3.amazonaws.com/can-uploader/simple-with-action-button-warning-svg1.svg" alt="warning" />
                </div>
                <p class="mr-2 text-base font-bold text-gray-800 dark:text-gray-100 mt-2 md:my-0">
                    {{ session('alert')['title'] }}</p>
                <p class="text-sm lg:text-base dark:text-gray-400 text-gray-600 lg:pt-1 xl:pt-0 sm:mb-0 mb-2 text-center sm:text-left">
                    {{ session('alert')['message'] }}</p>
            </div>
            <div class="flex xl:items-center lg:items-center sm:justify-end justify-center pr-4">
                <button class="focus:outline-none focus:text-gray-400 hover:text-gray-400 text-sm cursor-pointer text-gray-600 dark:text-gray-400" onclick="closeAlert()">X</button>
            </div>
        </div>
    </div>
    @endif
    <!-- Code block ends -->



    <form wire:submit.prevent="create">
        <div class="mb-6">
            <label for="name" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Nombre</label>
            <input wire:model.blur='form.name' type="text" id="name"
                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                placeholder="name@flowbite.com">
            @error('form.name')
            <p class="mt-2 text-sm text-red-600 dark:text-red-500"><span
                        class="font-medium">Oops!</span>{{ $message }}</p>
            @enderror
        </div>
        <div class="mb-6">
            <label for="email" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
                Correo electrónico</label>
            <input wire:model.live='form.email' type="email" id="email"
                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                placeholder="name@flowbite.com">
            @error('form.email')
            <p class="mt-2 text-sm text-red-600 dark:text-red-500"><span
                        class="font-medium">Oops!</span>{{ $message }}</p>
            @enderror
        </div>
        <div class="mb-6">
            <label for="password" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Contraseña</label>
            <input wire:model.live='form.password' type="password" id="password"
                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
            @error('form.password')
            <p class="mt-2 text-sm text-red-600 dark:text-red-500"><span
                        class="font-medium">Oops!</span>{{ $message }}</p>
            @enderror
        </div>

        <div class="mb-6">
            <label class="block mb-2 text-sm font-medium text-gray-900 dark:text-white" for="profile_photo_path">Subir
                foto</label>
            <input wire:model.live='form.profile_photo_path'
                class="block w-full text-sm text-gray-900 border border-gray-300 rounded-lg cursor-pointer bg-gray-50 dark:text-gray-400 focus:outline-none dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400"
                aria-describedby="profile_photo_path_help" id="profile_photo_path" type="file">
            <label class="mt-1 text-sm text-gray-500 dark:text-gray-300">subir en .JPG ó .PNG</label>
            @error('form.profile_photo_path')
            <p class="mt-2 text-sm text-red-600 dark:text-red-500"><span
                        class="font-medium">Oops!</span>{{ $message }}</p>
            @enderror
        </div>
        @if ($form->profile_photo_path && !$errors->has('form.profile_photo_path'))
        <div class="mb-6">
            <img class="m-auto h-56 w-56 border border-gray-200 rounded-lg bg-gray-50 dark:bg-gray-800 dark:border-gray-700" src="{{ $form->profile_photo_path->temporaryuRL() }}" alt="Vista previa">
        </div>
        @endif
        <div wire:loading wire:target="profile_photo_path" class="mb-6">
            <div class="px-3 py-1 m-auto text-sm font-medium leading-none text-center text-blue-800 bg-blue-200 rounded-full animate-pulse dark:bg-blue-900 dark:text-blue-200">
                Cargando foto...</div>
        </div>
        <div wire:loading wire:target="create" class="mb-6">
            <div class="px-3 py-1 m-auto text-sm font-medium leading-none text-center text-blue-800 bg-blue-200 rounded-full animate-pulse dark:bg-blue-900 dark:text-blue-200">
                Guardando</div>
        </div>
        <div class="mb-6">
            <div class="flex items-center justify-between flex-wrap">
                <button wire:loading.class="bg-gray-200" wire:loading.class.remove="dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800 hover:bg-blue-800" wire:loading.attr="disabled"
                    type="submit"
                    class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800 mb-2 sm:mb-0">
                    Guardar
                </button>
                <button type="button" wire:click="clear()"
                    class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800 mb-2 sm:mb-0">
                    Limpiar
                </button>
                {{-- sin alpine.js --}}
                {{-- <button wire:click.prevent="reloadList"
                    class="px-3 py-2 text-xs font-medium text-center text-white bg-blue-700 rounded-lg hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
                    <svg class="w-6 h-6 text-white dark:text-white" aria-hidden="true"
                        xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 20 18">
                        <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                            d="m1 14 3-3m-3 3 3 3m-3-3h16v-3m2-7-3 3m3-3-3-3m3 3H3v3"></path>
                    </svg>
                </button> --}}
                {{-- con alpine.js --}}
                <button type="button" @click="$dispatch('user-created')"
                    class="px-3 py-2 text-xs font-medium text-center text-white bg-blue-700 rounded-lg hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
                    <svg class="w-6 h-6 text-white dark:text-white" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 20 18">
                        <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="m1 14 3-3m-3 3 3 3m-3-3h16v-3m2-7-3 3m3-3-3-3m3 3H3v3"></path>
                    </svg>
                </button>
            </div>

        </div>
    </form>
</div>
