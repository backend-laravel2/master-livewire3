<!DOCTYPE html>
<html lang="es">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Eventos livewire 3</title>

        <script src="https://cdn.tailwindcss.com"></script>
    </head>
    <x-app-layout>
        <x-slot name="header">
            <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
                {{ __('EVENTOS LIVEWIRE V.3') }}
            </h2>
        </x-slot>

        <div class="py-4">
            <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
                <div class="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-2 lg:grid-cols-12 gap-4">
                    <!-- Primer div -->
                    <div class="lg:col-span-8 md:col-span-6 sm:col-span-12">
                        <div class="bg-white dark:bg-gray-800 overflow-hidden shadow-xl sm:rounded-lg">
                            {{-- Contenido del primer div --}}
                            {{-- @livewire('users-list',['lazy'=>true]) --}}
                            {{-- <livewire:users-list lazy /> --}}
                            {{-- <livewire:users-list lazy :search="date('Y')" /> PASANDDO PARAMETROS --}}
                            <livewire:users-list lazy search="" />
                        </div>
                    </div>

                    <!-- Segundo div -->
                    <div class="lg:col-span-4 md:col-span-6 sm:col-span-12">
                        <div class="bg-white dark:bg-gray-800 overflow-hidden shadow-xl sm:rounded-lg">
                            {{-- Contenido del segundo div --}}
                            <livewire:user-register-update />
                        </div>
                    </div>
                </div>
            </div>
        </div>



    </x-app-layout>
    <script>
        // Función para cerrar la alerta cuando se hace clic en el botón de cerrar
        function closeAlert() {
            var alertElement = document.getElementById('alert');
            if (alertElement) {
                alertElement.style.display = 'none';
            }
        }

    </script>
