<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('MODALES') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white dark:bg-gray-800 overflow-hidden shadow-xl sm:rounded-lg">
                <button type="button" x-data x-on:click="$dispatch('open-modal',{ name: 'componente1' })"
                    class="bg-indigo-600 px-4 py-2 text-base font-medium text-white shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2">
                    Open Modal 1
                </button>
                <button type="button" x-data x-on:click="$dispatch('open-modal',{ name: 'componente2' })"
                    class="bg-indigo-600 px-4 py-2 text-base font-medium text-white shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2">
                    Open Modal 2
                </button>
                <button type="button" x-data x-on:click="$dispatch('open-modal',{ name: 'userCreate' })"
                    class="bg-indigo-600 px-4 py-2 text-base font-medium text-white shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2">
                    Registrar usuario
                </button>
                {{-- <x-modal-custom title="Probando modal alpine.js"> --}}
                {{-- le agregamos nombre al componente para que el boton identificada cual de los n° componentes va activar --}}
                <x-modal-custom name="componente1" title="Probando componente 1">
                    {{-- <x-slot name="title">
                        Probando modal alpine.js
                    </x-slot> --}}
                    {{-- @slot('content')
                    Contenido del modal forma uno
                    @endslot --}}
                    <x-slot:content>
                        Contenido del modal forma dos
                    </x-slot:content>
                    {{-- <x-slot name="content">
                        Contenido del modal forma tres
                    </x-slot> --}}

                    <x-slot name="footer">
                        <x-secondary-button wire:click="$toggle('confirmingUserDeletion')" wire:loading.attr="disabled">
                            Nevermind
                        </x-secondary-button>

                        <x-danger-button class="ml-2" wire:click="deleteUser" wire:loading.attr="disabled">
                            Delete Account
                        </x-danger-button>
                    </x-slot>
                </x-modal-custom>
                <x-modal-custom name="componente2">
                    <x-slot name="title">
                        Probando modal alpine.js componente 2
                    </x-slot>
                    {{-- @slot('content')
                    Contenido del modal forma uno
                    @endslot --}}
                    <x-slot:content>
                        Contenido del modal forma dos componente 2
                    </x-slot:content>
                    {{-- <x-slot name="content">
                        Contenido del modal forma tres
                    </x-slot> --}}

                    <x-slot name="footer">
                        <x-secondary-button wire:click="$toggle('confirmingUserDeletion')" wire:loading.attr="disabled">
                            Nevermind
                        </x-secondary-button>

                        <x-danger-button class="ml-2" wire:click="deleteUser" wire:loading.attr="disabled">
                            Delete Account
                        </x-danger-button>
                    </x-slot>
                </x-modal-custom>
                <x-modal-custom name="userCreate">
                    <x-slot:content>
                        <livewire:user-register-update />
                    </x-slot:content>

                </x-modal-custom>
                <br>


            </div>
        </div>
    </div>
</x-app-layout>
