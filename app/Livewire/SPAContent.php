<?php

namespace App\Livewire;

use Livewire\Component;

class SPAContent extends Component
{
    public function render()
    {
        return view('livewire.s-p-a-content');
    }
}
