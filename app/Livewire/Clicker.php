<?php

namespace App\Livewire;

use App\Models\User;
use Livewire\Attributes\Rule;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;

class Clicker extends Component
{
    use WithPagination;
    use WithFileUploads;
    // public $name, $email, $password;
    #[Rule('required|min:3|max:50')]
    public $name = '';

    #[Rule('required|email|unique:users')]
    public $email = '';

    #[Rule('required|min:2')]
    public $password = '';
    #[Rule('nullable|sometimes|image|max:1024')]
    public $profile_photo_path;

    public function render()
    {
        $title = "Usuarios";
        $users = User::paginate(5);
        return view('livewire.clicker', ['title' => $title, 'users' => $users]);
    }
    public function createNewUser()
    {
        sleep(5);
        // $faker = Faker::create();
        // User::create([
        //     'name' => $faker->name,
        //     'email' => $faker->email,
        //     'password' => bcrypt('admin123'),
        // ]);
        // $this->validate([
        //     'name' => 'required|min:3',
        //     'email' => 'email|required',
        //     'password' => 'required',
        // ]);
        // User::create([
        //     'name' => $validated['name'],
        //     'email' => $validated['email'],
        //     'password' => Hash::make($validated['password']),
        // ]);
        $validated = $this->validate();
        if ($this->profile_photo_path) {
            $validated['profile_photo_path'] = $this->profile_photo_path->store('uploads', 'public');
        }
        User::create($validated);
        request()->session()->flash('success', 'Registro con éxito');
        $this->reset(['name', 'email', 'password', 'profile_photo_path']);
    }
}
