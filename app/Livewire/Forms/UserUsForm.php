<?php

namespace App\Livewire\Forms;

use App\Models\User;
use Livewire\Attributes\Rule;
use Livewire\Form;

class UserUsForm extends Form
{

    #[Rule('required|min:3|max:255|unique:users,name')]
    public $name = '';

    #[Rule('required|email|unique:users,email')]
    public $email = '';

    #[Rule('required|min:3')]
    public $password = '';

    // validación personalizado

    #[Rule('required|image|mimes:jpeg,png,gif|max:2048', as :'Foto de perfil')]
    public $profile_photo_path;

    // #[Rule(['images.*' => ['required', 'image', 'max:1024']])]
    // #[Rule(['images.*' => 'image|max:1024'])]
    // public $images = [];


    public function store()
    {
        $validated = $this->validate();
        // dd($validated);
        // dump($validated);
        if ($this->profile_photo_path) {
            $validated['profile_photo_path'] = $this->profile_photo_path->store('users', 'public');
        }
        // dd($validated['profile_photo_path']);
        $user = User::create($validated);
        $this->reset('name', 'password', 'email', 'profile_photo_path');

        return $user;

    }
}
