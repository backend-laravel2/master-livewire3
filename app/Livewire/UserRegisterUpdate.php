<?php

namespace App\Livewire;

use App\Livewire\Forms\UserUsForm;
use Livewire\Component;
use Livewire\WithFileUploads;

class UserRegisterUpdate extends Component
{
    use WithFileUploads;

    public UserUsForm $form;
    // Ciclos de vida de vida y hook forma 2 algunos ejemplos
    public function updated($property, $value)
    {
        // dd($property);
        if ($property === 'form.name') {
            // dd($property);
            $this->form->name = strtoupper($this->form->name);
        }
    }
    // Ciclos de vida de vida y hook forma 2 por nombre de propiedad NO FUNCIONA CON FORM
    // public function updatedName()
    // {
    //     $this->form->name = strtoupper($this->form->name);
    // }
    public function create()
    {
        // sleep(5);
        $user = $this->form->store();
        // $user = User::create($this->all());
        $this->dispatch('user-created', $user);
        $this->dispatch('close-modal'); //envia el evento y es escucha con alpine.js en el componente de modal
        $this->alert('Registrado', 'con éxito', 'green');

    }
    public function reloadList()
    {
        $this->dispatch('user-created');
    }
    public function render()
    {
        return view('livewire.user-register-update');
    }
    public function alert($title = "", $message = "", $color = "")
    {
        $data = [
            'title' => $title,
            'message' => $message,
            'color' => $color,
        ];
        session()->flash('alert', $data);
    }
    public function clear()
    {
        $this->form->reset('name', 'email', 'password');
    }

}
