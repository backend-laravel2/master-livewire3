<?php

namespace App\Livewire;

use App\Models\User;
use Livewire\Attributes\Computed;
use Livewire\Attributes\On;
use Livewire\Attributes\Url;
use Livewire\Component;
use Livewire\WithPagination;

class UsersList extends Component
{
    use WithPagination;
    public User $selectedUser;
    #[Url( as :'s')]
    // #[Url( as :'s', history: true, keep: false)]
    public $search;

    #[On("user-created")]
    public function updateUserList($user = null)
    {
        // dd($user);
    }
    public function mount($search = null)
    {
        $this->search = $search;
        unset($this->users); //Para limpiar caché de computed
    }public function placeholder()
    {
        return view('users.placeholder');
    }
    // Para Propiedades calculadas computadas
    #[Computed()]
    public function users()
    {
        return \App\Models\User::latest()->where('name', 'like', "%{$this->search}%")->paginate(5);
    }
    // public function render()
    // {
    //     // sleep(5);
    //     $users = \App\Models\User::latest()->where('name', 'like', "%{$this->search}%")->paginate(5);
    //     return view('livewire.users-list', compact('users'));
    // }
    public function viewUser(User $user)
    {
        // dd($user);
        $this->selectedUser = $user;
        $this->dispatch('open-modal', name: 'user-details');
    }
}
