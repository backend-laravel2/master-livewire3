<?php

namespace App\Livewire;

use App\Models\Todo;
use Exception;
use Livewire\Attributes\Rule;
use Livewire\Component;
use Livewire\WithPagination;

class TaskList extends Component
{
    use WithPagination;
    #[Rule('required|min:3|max:50')]
    public $name = '';
    public $search;
    #[Rule('required|min:3|max:50')]
    public $editingTodoName;
    public $editingTodoId;
    public $alertType = "";
    public $alertColor = "";
    public function create()
    {
        $validated = $this->validateOnly('name');
        Todo::create($validated);
        $this->reset('name');
        // session()->flash('success', 'Creado exitosamente');
        $this->alert('Genial', 'Registro guardado con éxito!', 'blue');

        $this->resetPage();
    }
    public function toggle(Todo $todo)
    {
        $todo->completed = !$todo->completed;
        $todo->save();

    }
    public function edit(Todo $todo)
    {
        $this->editingTodoId = $todo->id;
        $this->editingTodoName = $todo->name;
    }
    public function cancelEdit()
    {
        $this->reset('editingTodoId', 'editingTodoName');
    }
    public function update()
    {
        $validated = $this->validateOnly('editingTodoName');

        Todo::find($this->editingTodoId)->update([
            'name' => $validated['editingTodoName'],
            // 'name' => $this->editingTodoName,
        ]);
        $this->alert('Genial', 'Actualizado con éxito', 'teal');

        $this->cancelEdit();
    }
    public function delete($todoId)
    {
        try {
            Todo::findOrfail($todoId)->delete();
        } catch (Exception $e) {
            $this->alert('Error', 'Error en la eliminación de la tarea', 'red');
            return;
        }
        $this->alert('Excelente', 'Tarea eliminado con éxito', 'green');
        // $this->resetPage();
    }

    public function render()
    {

        return view('livewire.task-list', ['todos' => Todo::latest()->where('name', 'like', "%{$this->search}%")->paginate(5)]);
    }
    public function alert($title = "", $message = "", $color = "")
    {
        $data = [
            'title' => $title,
            'message' => $message,
            'color' => $color,
        ];
        session()->flash('alert', $data);

    }
}
