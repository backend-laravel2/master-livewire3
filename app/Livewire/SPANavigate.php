<?php

namespace App\Livewire;

use Livewire\Component;

class SPANavigate extends Component
{
    public function render()
    {
        return view('livewire.s-p-a-navigate');
    }
}
