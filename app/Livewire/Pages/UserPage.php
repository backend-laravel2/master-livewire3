<?php

namespace App\Livewire\Pages;

use App\Models\User;
use Livewire\Attributes\Title;
use Livewire\Component;

#[Title("Usuarios")]
class UserPage extends Component
{
    public User $user;
    // public function render()
    // {
    //     return view('livewire.pages.user-page');
    // }
}
