<?php

namespace App\Livewire\Pages;

use Livewire\Attributes\Title;
use Livewire\Component;

// #[Layout("layouts.app")] Personalizado layout
#[Title("Inicio")]
class HomePage extends Component
{
    public function render()
    {
        return view('livewire.pages.home-page');
    }
}
